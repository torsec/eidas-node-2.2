##
## builder
##

FROM maven:3-jdk-8-alpine AS builder

WORKDIR /sources
COPY . .

WORKDIR /sources/EIDAS-Parent
RUN mvn clean install -P NodeOnly,DemoToolsOnly

##
## executor
##

FROM tomcat:8-jre8-alpine

RUN mkdir -p /opt/eidas/logs

COPY --from=builder /sources/EIDAS-Config/server /opt/eidas/conf
COPY --from=builder /sources/EIDAS-Config/keystore /opt/eidas/keystore

RUN { \
        echo 'JAVA_OPTS="${JAVA_OPTS} -DEIDAS_CONFIG_REPOSITORY=/opt/eidas/conf/"' ; \
        echo 'JAVA_OPTS="${JAVA_OPTS} -DIDP_CONFIG_REPOSITORY=/opt/eidas/conf/idp/"' ; \
        echo 'JAVA_OPTS="${JAVA_OPTS} -DLOG_HOME=/opt/eidas/log"' ; \
        echo 'JAVA_OPTS="${JAVA_OPTS} -DSPECIFIC_CONNECTOR_CONFIG_REPOSITORY=/opt/eidas/conf/specificConnector/"' ; \
        echo 'JAVA_OPTS="${JAVA_OPTS} -DSPECIFIC_PROXY_SERVICE_CONFIG_REPOSITORY=/opt/eidas/conf/specificProxyService/"' ; \
        echo 'JAVA_OPTS="${JAVA_OPTS} -DSP_CONFIG_REPOSITORY=/opt/eidas/conf/sp/"' ; \
    } >> ${CATALINA_HOME}/bin/setenv.sh

COPY --from=builder /sources/EIDAS-IdP-1.0/target/IdP.war ${CATALINA_HOME}/webapps/
COPY --from=builder /sources/EIDAS-Node/target/EidasNode.war ${CATALINA_HOME}/webapps/
COPY --from=builder /sources/EIDAS-SP/target/SP.war ${CATALINA_HOME}/webapps/
COPY --from=builder /sources/EIDAS-SpecificConnector/target/SpecificConnector.war ${CATALINA_HOME}/webapps/
COPY --from=builder /sources/EIDAS-SpecificProxyService/target/SpecificProxyService.war ${CATALINA_HOME}/webapps/

# vim: ft=dockerfile
