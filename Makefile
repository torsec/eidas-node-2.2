VOL_NAME := "eidas-22-mvn-repo"
VOL_NAME_EXISTS := $(shell docker volume ls | grep $(VOL_NAME) | wc -l)
CURRENT_DIR := $(shell pwd)
REPO := torsec/eidas-node
TAG := 2.2

default: build-wars

mvn-repo:
ifeq ($(VOL_NAME_EXISTS),0)
	$(info Creating volume $(VOL_NAME))
	docker volume create --name $(VOL_NAME)
else
	$(info Volume $(VOL_NAME) already defined)
endif

build-wars: mvn-repo
	docker run -ti --rm \
		-v $(VOL_NAME):/root/.m2 \
		-v "$(CURRENT_DIR):/sources" \
		-w /sources/EIDAS-Parent \
		maven:3-jdk-8-alpine \
		mvn clean install -PNodeOnly,DemoToolsOnly

build-devel: build-wars
	docker build --tag $(REPO):$(TAG)-devel -f Dockerfile.devel .

run-devel: build-devel
	docker run -ti --rm -p "8080:8080" \
		--add-host eidasnode:127.0.0.1 \
		--add-host specificproxyservice:127.0.0.1 \
		--add-host specificconnector:127.0.0.1 \
		$(REPO):$(TAG)-devel

build:
	docker build --tag $(REPO):$(TAG) -f Dockerfile .

run: build
	docker run -ti --rm -p "8080:8080" \
		--add-host eidasnode:127.0.0.1 \
		--add-host specificproxyservice:127.0.0.1 \
		--add-host specificconnector:127.0.0.1 \
		$(REPO):$(TAG)

clean:
	docker rmi -f $(shell docker images "$(REPO)*" -q)

deep-clean: clean
	docker rmi -f $(shell docker images -q -f dangling=true)

list:
	docker images "$(REPO)*"
