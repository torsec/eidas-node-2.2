# eIDAS Node 2.2

This is a mirror for the official eIDAS Node v2.2 available on the
[CEF Digital web site](https://ec.europa.eu/cefdigital/wiki/display/CEFDIGITAL/eIDAS-Node+version+2.2).

## Use Docker images

*   Build the container image

        $ make build

*   Build the container image without downloading each time the Maven
    dependencies (suitable for development)

        $ make build-devel

*   Run the whole environment

        $ make run

*   Run the whole development environment

        $ make run-devel

Once the container is up and running, update your `/etc/hosts` with the line

    <YOUR.CONTAINER.IP.ADDRESS>    eidasnode specificproxyservice specificconnector

then open with your browser

    http://eidasnode:8080/SP

and enjoy it!
